package com.example.lili_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiliSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(LiliSpringApplication.class, args);
	}

}
